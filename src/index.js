'use strict';
var Alexa = require('alexa-sdk');
var APP_ID = undefined;  // TODO replace with your app ID (OPTIONAL).
var name = "Lisa";
var HELP_MESSAGE = name + " would love to hear how you feel.";
var negativeFeelings = ["stressed",
    "demotivated",
    "irritated",
    "powerless",
    "alone",
    "tired",
    "aggressive",
    "infuriated",
    "hateful",
    "sad",
    "bad",
    "angry",
    "disappointed",
    "down",
    "discouraged",
    "hopeless",
    "desillusioned",
    "disapointed",
    "annoyed",
    "upset",
    "resentful",
    "unfuriated",
    "bitter",
    "useless",
    "incapable",
    "doubtful",
    "uncertain",
    "vulnerable",
    "embarassed",
    "empty",
    "lonely",
    "exhausted",
    "diminished",
    "wornout"
    ];

var positiveFeelings = [
    "confident",
    "calm",
    "inspired", 
    "good",
    "loved",
    "powerful",
    "determined",
    "dynamic",
    "proud",
    "energetic",
    "optimistic",
    "hopeful",
    "appreciated",
    "relaxed",
    "peaceful",
    "happy",
    "curious",
    "engrossed",
    "excited", 
    "enthusiastic",
    "grateful",
    "positive",
    "great"
    ];

exports.handler = function (event, context, callback) {
    var alexa = Alexa.handler(event, context);
    alexa.APP_ID = APP_ID;
    alexa.dynamoDBTableName = 'AlexaFeelingsUser';
    alexa.registerHandlers(handlers);

    alexa.execute();
};


var handlers = {
    // according to shceme
    'LaunchRequest': function () {

        console.log("SessionEndedRequest");
        if (Object.keys(this.attributes).length === 0) {
            var tts = "Hi, I’m " + name + " - I make sure you are ok. You can talk to me whenever you feel exhausted, stressed or discouraged. What's your name?";
            this.emit(':ask', tts);
        } else {
            var userName = this.attributes['name'];
            if( typeof userName !== 'undefined' ) {
                this.emit(':ask', 'Hi, ' + userName + ", how are you feeling today?");
            } else {
                var tts = "Hi, I’m " + name + " - I make sure you are ok. You can talk to me whenever you feel exhausted, stressed or discouraged. What's your name?";
                this.emit(':ask', tts);
            }
        }
    },

    'shareFeelingsIntent': function () {
        var feeling = this.event.request.intent.slots.feelingsslot.value;
        console.error("shareFeelingsIntent: feeling= " + feeling);
        if (negativeFeelings.indexOf(feeling) > -1) {
            this.attributes["question"] = "talkedToColleague"
            var userName = this.attributes['name'];
            var question = "Have you talked to your colleagues about it?";
            var response = "I am sorry to hear that " + userName + ". It sounds like you may benefit from some support. " + question;
            var reprompt = question;
            this.emit(':ask', response, reprompt);
        } else {
            this.emit(":tell", "So happy to hear, keep up the good work.")
        }
    },

    'AMAZON.YesIntent': function () {
        if (this.attributes["question"] == "talkedToColleague") {
            this.attributes["question"] = "shareActivities";
            var question = "what are some things you enjoy doing?";
            var response = "Great, keep doing that. It is important to open up to your colleagues about how you feel, so they can support you. Now," + question;
            this.emit(':ask', response, question);

        } else if (this.attributes["question"] == "madeTimeForActivity") {
            this.attributes["question"] = "scheduleActivity";
            var question = "Are you open to scheduling in one pleasant activity you can do each day?";
            var response = "Great, keep doing that. You might be able to care more for company if you take time to refresh. " + question;
            this.emit(':ask', response, question);
        } else if (this.attributes["question"] == "scheduleActivity") {
            this.attributes["question"] = "activitiesToDo"
            var response = "Great, that is very important. What pleasant activity can you schedule today?";
            var reprompt = "What pleasant activity can you schedule today?";
            this.emit(':ask', response, reprompt);
        }
    },

    'AMAZON.NoIntent': function () {
        if (this.attributes["question"] == "talkedToColleague") {
            var response = "I understand you may have reserves. However, \
             it might be helpful to open up to your colleagues about how you feel, so they can support you. Now, what are some things you enjoy doing?";
            var reprompt = "what are some things you enjoy doing?";
            this.attributes["question"] = "shareActivities";
            this.emit(':ask', response, reprompt);
        } else if (this.attributes["question"] == "madeTimeForActivity") {
            this.attributes["question"] = "scheduleActivity"
            var response = "I see. You might be able to care more for company if you take time to refresh.\
             Are you open to scheduling in one pleasant activity you can do each day?";
            var reprompt = "Are you open to scheduling in one pleasant activity you can do each day?";
            this.emit(':ask', response, reprompt);

        } else if (this.attributes["question"] == "scheduleActivity") {
            this.attributes["question"] = "invalid"
            var response = "I see. I'm sorry that I can't help you then, please consider talking to someone. have a good day."
            this.emit(':tell', response);

        }
    },

    'GivesActivity': function () {
        console.error("GivesActivity: question= " + this.attributes["question"]);
        if (this.attributes["question"] == "shareActivities") {
            this.attributes["question"] = "madeTimeForActivity";
            var response = "Have you made time for these lately?";
            var reprompt = response;
            this.emit(':ask', response, reprompt);
        } else {
            this.emit(":tell", "That sounds like a great plan. it is just as important for you to take time for yourself so that you don`t burnout caring for your company.");
        }
    },

    'GivesName': function () {
        //parse name
        var name = this.event.request.intent.slots.first_nameslot.value;
        this.attributes["name"] = name;
        this.emit(':ask', 'Hi, ' + name + ", how are you feeling today?");
    },

    'SessionEndedRequest': function () {
        console.log("SessionEndedRequest");
        console.log('session ended!');
        this.attributes["question"] = "invalid";
        this.emit(':saveState', true);
    },

    'AMAZON.HelpIntent': function () {
        var speechOutput = HELP_MESSAGE;
        var reprompt = "how are you feeling today?";
        this.emit(':ask', speechOutput, reprompt);
    },
    'AMAZON.CancelIntent': function () {
        this.attributes["question"] = "";
        this.emit(':tell', "Have a good day!");
    },
    'AMAZON.StopIntent': function () {
        this.attributes["question"] = "";
        this.emit(':tell', "Have a good day!");
    },

    'AMAZON.StartOverIntent': function () {
        console.log('startOverIntent!');
        this.attributes["question"] = undefined;
        this.attributes["name"] = undefined;
        this.emit(':saveState', true);
    },

    'Unhandled': function () {
        this.emit(':ask', HELP_MESSAGE, HELP_MESSAGE);
    }
};